
Variables 
1. Déclarez une variable age et attribuez-lui votre âge.
2. Créez une variable nom et attribuez-lui votre nom. Affichez le résultat.
3. Combinez nom et age dans une chaîne de caractères et affichez le résultat.

Conditions :
4. Demandez à l'utilisateur de saisir un nombre. Affichez "Pair" s'il est pair, sinon affichez "Impair".
5. Demandez à l'utilisateur de saisir un nombre. Affichez "Positif", "Négatif" ou "Nul" en fonction du nombre saisi.
6. Écrivez un programme qui compare deux nombres et affiche le plus grand.

Boucles :
7. Utilisez une boucle for pour afficher les chiffres de 1 à 5.
8. Modifiez le programme précédent pour afficher la somme de ces chiffres.
9. Utilisez une boucle while pour afficher les carrés des nombres de 1 à 3.

Tableaux (Listes) :
10. Créez une liste de fruits et affichez chaque élément.
11. Ajoutez un nouveau fruit à la liste et réaffichez-la.
12. Utilisez une boucle for pour afficher chaque caractère d'un mot.

Conditions et Boucles combinées :
13. Écrivez un programme qui affiche les nombres de 1 à 10, mais remplacez les multiples de 3 par "Fizz" et les multiples de 5 par "Buzz".
14. Demandez à l'utilisateur de deviner un nombre. Affichez "Correct !" s'il trouve le bon nombre, sinon donnez des indices pour guider la recherche.

Tableaux (Listes) et Boucles :
15. Créez une liste de nombres. Utilisez une boucle for pour afficher chaque nombre multiplié par 2.
16. Filtrer les nombres pairs de la liste créée précédemment et les stocker dans une nouvelle liste.
17. Utilisez une boucle while pour trouver la somme des nombres de la liste créée au point 15.

Conditions et Tableaux (Listes) :
18. Créez une liste de noms. Utilisez une boucle for pour afficher les noms qui commencent par la lettre "A".
19. Ajoutez une condition pour afficher également les noms qui commencent par la lettre "M".
20. Comptez le nombre total de lettres dans tous les noms de la liste.

