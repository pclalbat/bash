# TP Vagrant

## BRIEF 1

### Intégralité du tp1
```bash
#Création du sossier de travail
mkdir $HOME\Desktop\vagrant
cd $HOME\Desktop\vagrant
# Vagrant init permet de créer un vagrantfile avec toute l'explication de l'utilisation du fichier
Vagrant init
# Vagrant Validate permet de valider la syntaxe du vagrant file, afin de limiter les erreur l'ors le déxecution de celui-ci.
Vagrant validate
# Vagrnat up permet de démarrer la liste des machines virtuelles avec leur configuration  définie dans le vagrantfile.
Vagrant up
# Vagrant status permet d'avoir des nformations concenant l'execution d'une machine virtuelles en particulier
Vagrant status
#Vagrant global status permet d'avoir des informations concenant l'execution des machines virtuelles
Vagrant global-status
#Vagrant ssh permet de de se connecter en ssh à une machine virtuelle
Vagrant ssh
#Vagrant halt permet d'arrêter les machines virtuelles
Vagrant halt
#Vagrant destroy permet d'arrêter et supprimer la machine virtuelle
Vagrant destroy
# Permet de télécharger la box et de copier le vagrant file nécéssaire à son déploiement, Vagrant up permet de lancer la machine.
Vagrant box add ubuntu/trusty64 | Vagrant up
