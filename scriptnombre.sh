#/bin/bash

#Nombre aléatoire entre 1 et 1000
#nombre=$(shuf -i 1-1000 -n 1)

#la variable random va naturellement j'usqu'a 32767, on la "recale" à 1000, pour que 32767 soit égal à 1000.
nombre=$(($RANDOM*1000/32767))


while true; do
    # Demande à l'utilisateur d'entrer son estimation
    read -p "Entrez votre supposition (un nombre entre 1 et 1000): " nbuser

    if (( $nbuser < $nombre )); then
        echo "Trop petit, essaie encore!"
    elif (( $nbuser > $nombre )); then
        echo "Trop grand,essaie encore."


    else
        echo "Bravo, c'est exactement le bon nombre !"
        yes $nbuser
        
    fi
done
