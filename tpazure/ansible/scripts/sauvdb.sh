#!/bin/bash

# Date actuelle
current_date=$(date +"%Y%m%d")

# Nom du fichier de sauvegarde
backup_file="backup_postgres_$current_date.sql"

# Chemin du journal de sauvegarde
log_file="/var/log/postgres_backup.log"

# Répertoire de destination sur le partage NFS
nfs_backup_dir="/mnt/nfs/backups"

# Exporter la base de données PostgreSQL
pg_dump -U <odoo_user> -h localhost -p 5432 <odoo_db> > "$backup_file" 2>> "$log_file"

# Vérifier si la sauvegarde a réussi
if [ $? -eq 0 ]; then
    echo "$(date +"%Y-%m-%d %T") : La sauvegarde de la base de données PostgreSQL a réussi." >> "$log_file"
    # Copier le fichier de sauvegarde sur le partage NFS
    cp "$backup_file" "$nfs_backup_dir" >> "$log_file" 2>&1
    # Vérifier si la copie a réussi
    if [ $? -eq 0 ]; then
        echo "$(date +"%Y-%m-%d %T") : La sauvegarde a été copiée avec succès sur le partage NFS." >> "$log_file"
    else
        echo "$(date +"%Y-%m-%d %T") : Erreur lors de la copie de la sauvegarde sur le partage NFS." >> "$log_file"
    fi
else
    echo "$(date +"%Y-%m-%d %T") : Erreur lors de la sauvegarde de la base de données PostgreSQL." >> "$log_file"
fi
