---
- name: Installer Docker sur srvdb
  hosts: srvweb1
  become: true
 
  tasks:
    - name: Mise à jour du cache des paquets
      apt:
        update_cache: yes
      when: ansible_os_family == 'Debian'
 
    - name: Install Docker
      apt:
        name: docker.io
        state: present
      when: ansible_os_family == 'Debian'

    - name: Install Docker
      apt:
        name: docker-compose
        state: present
      when: ansible_os_family == 'Debian'
 
    - name: Start and enable Docker service
      systemd:
        name: docker
        enabled: yes
        state: started

    - name: Add current user to docker group
      become: true
      shell: usermod -aG docker AzureVmUser

- name: Déploiement du service NFS
  hosts: srvdb1
  become: true

  tasks:
    - name: Mise à jour des paquets
      apt:
        update_cache: yes
        upgrade: yes

    - name: Add current user to docker group
      become: true
      shell: |
        groupadd docker
        usermod -aG docker AzureVmUser

    - name: Installation du serveur NFS
      apt:
        name: nfs-kernel-server
        state: present

    - name: Création du répertoire à partager
      file:
        path: /srv/nfs/share
        state: directory
        owner: AzureVmUser
        group: docker
        mode: '0750'

    - name: Configuration du partage NFS
      become: true
      lineinfile:
        path: /etc/exports
        line: "/srv/nfs/share IP_SRVNFS(rw,sync,no_subtree_check)"
        state: present

    - name: Redémarrage du service NFS
      systemd:
        name: nfs-kernel-server
        state: restarted
        enabled: yes

- name: Créer un dossier pour docker-compose sur srvweb1
  hosts: srvweb1
  become: true

  tasks:
    - name: Créer un dossier
      ansible.builtin.file:
        path: /home/AzureVmUser/ansible/docker-compose  # Spécifie le chemin du dossier à créer
        state: directory
- name: Créer un dossier pour le partage nfs sur srvweb1
  hosts: srvweb1
  become: true

  tasks:
    - name: Créer un dossier
      ansible.builtin.file:
        path: /mnt/nfs  # Spécifie le chemin du dossier à créer
        state: directory
        
- name: Configuration du partage NFS sur srvdb1
  hosts: srvdb1
  tasks:
    - name: Récupérer l'adresse IP de srvdb1
      ansible.builtin.command: hostname -i
      register: srvdb1_ip

- name: Configuration du partage NFS sur srvweb1
  hosts: srvweb1
  tasks:
    - name: Récupérer l'adresse IP de srvdb1 depuis le résultat de la commande sur srvdb1
      set_fact:
        nfs_server_ip: "{{ hostvars['srvdb1'].srvdb1_ip.stdout }}"
    
    - name: Configuration du partage NFS
      become: true
      lineinfile:
        path: /etc/fstab
        line: "{{ nfs_server_ip }}:/srv/nfs/share /mnt/nfs nfs defaults 0 0"
        state: present

- name: Créer un réseau Docker sur le serveur distant
  hosts: srvweb1
  become: true

  tasks:
    - name: Créer un réseau Docker
      ansible.builtin.shell:
        cmd: docker network create mon-reseau

- name: Déployer docker-compose postgre sur le serveur distant
  hosts: srvweb1
  become: true

  tasks:
    - name: Copier le fichier docker-compose.yml
      copy:
        src: /home/AzureVmUser/tpdocker/ansible/docker-compose/docker-compose.yml
        dest: /home/AzureVmUser/ansible/docker-compose/docker-compose.yml

    - name: Exécuter docker-compose up
      ansible.builtin.shell:
        cmd: docker-compose -f /home/AzureVmUser/ansible/docker-compose/docker-compose.yml up -d
        chdir: /home/AzureVmUser/ansible/docker-compose/
      become: true

- name: Sauvegarde de la base de données PostgreSQL et configuration de la crontab
  hosts: srvweb1
  become: true

  vars:
    script_name: sauvdb.sh
    nfs_backup_dir: "/mnt/nfs/backups"
    odoo_user: "odoo_user"
    odoo_db: "odoo_db"

- name: Créer un dossier sur le serveur distant
  hosts: srvweb1
  become: true
  tasks:
    - name: Créer un dossier
      ansible.builtin.file:
        path: /home/AzureVmUser/ansible/scripts  # Spécifie le chemin du dossier à créer
        state: directory
  tasks:
    - name: Copier le script de sauvegarde sur le serveur
      copy:
        src: /home/AzureVmUser/tpdocker/ansible/scripts/sauvdb.sh
        dest: /usr/local/bin/sauvdb.sh
        mode: '0755'

    - name: Créer le répertoire de sauvegarde sur le serveur NFS
      file:
        path: /mnt/nfs/backups
        state: directory
        owner: "AzureVmUser"
        group: "AzureVmUser"
        mode: '0755'

    - name: Ajouter une tâche cron pour la sauvegarde de la base de données
      cron:
        name: "Backup PostgreSQL"
        minute: "0"
        hour: "0"
        job: "/usr/local/bin/sauvdb.sh"
        state: present
