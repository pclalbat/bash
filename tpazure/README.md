# TP Azure

## Sommaire
 ### Explication de l'infrastructure
 ### Déploiement de l'infrastructure dans Azure grâce à azure cli
 ### Configuration de l'infratucture
 ### 

# Partie °1 Expication de l'infrastructure

### la finalitée de ce projet est d'avoir l'application odoo fonctionnelle dans azure.
### 1. Prérequis Azure

Avant de procéder au déploiement de l'infrastructure, il est nécessaire de disposer des éléments suivants :

- Un compte Azure pour accéder à la plateforme de cloud computing de Microsoft.
- Un groupe de ressources avec la convention de nommage "OCC_ASD_" pour regrouper les ressources liées au projet.
- Un stockage nécessaire pour Azure CLI, qui sera utilisé pour déployer l'infrastructure.

### 2. Déploiement de l'Infrastructure

Une fois les prérequis remplis, nous utiliserons Azure CLI pour déployer l'infrastructure nécessaire à l'hébergement de l'application Odoo. Cela impliquera la création et la configuration des ressources suivantes gràce au script nommé "creainfra_odoo" (à executer dans azure cli):

```bash

# Login to Azure


# Create resource group

#execute this script

read -p "Entrez votre prénom, utilisé dans le groupe de ressource. Exemple : si vous vous appelez Rantanplan, écrivez Rantanplan, pour le groupe OCC_ASD_Rantaplan " nom
echo "OCC_ASD_"$nom""
read -p "Entrez votre numéro (de 1 à 15)" XX
echo "$XX"
read -p "Entrez le nom d'utilisateur commun à vos machines, en minuscules." username
echo "$username"
username=$(echo "$username" | tr 'A-Z' 'a-z')
echo "Nom d'utilisateur converti en minuscules : $username . En minuscules RTFM"
read -p "Entrez la taille des disques des vms en GB" diskSizeGB
echo "La taille des disques sera de $diskSizeGB GB"
keyname="$nom"_ssh_key
subscription_id=$(az account list --output json | jq '.[].id' -r)
ressource_group_name="OCC_ASD_$nom"
# Create network security group
az network nsg create --resource-group OCC_ASD_$nom --name NSG_OCC_ASD_$nom

# Create virtual network
az network vnet create --resource-group OCC_ASD_$nom --name Vnet_OCC_ASD_$nom --address-prefix 10.0.$XX.0/24 # --network-security-group NSG_OCC_ASD_$nom

#Create subnet Admin
az network vnet subnet create -g OCC_ASD_$nom --name Subnet_Vnet_OCC_ASD_"$nom"_Admin --vnet-name Vnet_OCC_ASD_$nom  --address-prefix 10.0.$XX.0/28 # --network-security-group "" #NSG_OCC_ASD_$nom
az network vnet subnet create -g OCC_ASD_$nom --name Subnet_Vnet_OCC_ASD_"$nom"_web --vnet-name Vnet_OCC_ASD_$nom --address-prefix 10.0.$XX.16/28 # --network-security-group "" #NSG_OCC_ASD_$nom
az network vnet subnet create -g OCC_ASD_$nom --name Subnet_Vnet_OCC_ASD_"$nom"_db --vnet-name Vnet_OCC_ASD_$nom --address-prefix 10.0.$XX.32/28 # --network-security-group "" # NSG_OCC_ASD_$nom

#Create public ip address
az network public-ip create --name PubIpAdm$nom --resource-group OCC_ASD_$nom --location francecentral --sku standard
az network public-ip create --name PubIpWeb$nom --resource-group OCC_ASD_$nom --location francecentral --sku standard

#Create Network interface cards
az network nic create --resource-group OCC_ASD_$nom --name nicAdm --location francecentral --subnet Subnet_Vnet_OCC_ASD_"$nom"_Admin --vnet-name Vnet_OCC_ASD_$nom --network-security-group NSG_OCC_ASD_$nom  --public-ip-address PubIpAdm$nom
az network nic create --resource-group OCC_ASD_$nom --name nicWeb --location francecentral --subnet Subnet_Vnet_OCC_ASD_"$nom"_web --vnet-name Vnet_OCC_ASD_$nom --network-security-group NSG_OCC_ASD_$nom  --public-ip-address PubIpWeb$nom
az network nic create --resource-group OCC_ASD_$nom --name nicDb --location francecentral --subnet Subnet_Vnet_OCC_ASD_"$nom"_db --vnet-name Vnet_OCC_ASD_$nom --network-security-group NSG_OCC_ASD_$nom


#Create disks

az disk create --name DiskAdmin --resource-group OCC_ASD_$nom --location francecentral --size-gb $diskSizeGB --sku StandardSSD_LRS --zone 1
az disk create --name Disksrvweb --resource-group OCC_ASD_$nom --location francecentral --size-gb $diskSizeGB --sku StandardSSD_LRS --zone 1
az disk create --name Disksrvdb --resource-group OCC_ASD_$nom --location francecentral --size-gb $diskSizeGB --sku StandardSSD_LRS --zone 1

#Create Vms
#VM1
az vm create --resource-group OCC_ASD_$nom --name CliduckerAdm --image Debian11 --attach-data-disk DiskAdmin --admin-username $username --generate-ssh-keys --nics nicAdm --location francecentral --size Standard_B1s # --subnet Subnet_Vnet_OCC_ASD_"$nom"_Admin  --vnet-name Vnet_OCC_ASD_$nom  --location francecentral --size Standard_B1s --public-ip-address PubIpAdm$nom
#VM2
az vm create --resource-group OCC_ASD_$nom --name SrvduckerWeb --image Debian11 --attach-data-disk Disksrvweb --admin-username $username --generate-ssh-keys --nics nicWeb --location francecentral --size Standard_D2s_v3 # --subnet Subnet_Vnet_OCC_ASD_"$nom"_web --vnet-name Vnet_OCC_ASD_$nom --location francecentral --size Standard_D2s_v3 --public-ip-address PubIpWeb$nom
#VM3
az vm create --resource-group OCC_ASD_$nom --name SrvduckerDb --image Debian11 --attach-data-disk Disksrvdb --admin-username $username --generate-ssh-keys --nics nicDb --location francecentral --size Standard_B1s # --subnet Subnet_Vnet_OCC_ASD_"$nom"_db --vnet-name Vnet_OCC_ASD_$nom --location francecentral --size Standard_B1s --public-ip-address ""


```




# Partie °2

## Voici le résultat que vous devriez avoir suite à l'execution du script (ne me demandez pas pourquoi ça part dans tous les sens, c'est généré par azure.)

![Infra azure](tpazure/infra_azure_cli.svg)

## Nous allons maintenant 



# Partie °3 Ansible Playbook Explication

## Installation de Docker sur srvweb1

- Ce bloc de tâches vise à installer Docker sur le serveur nommé srvweb1.

### Tâches:
1. **Mise à jour du cache des paquets**: Met à jour le cache des paquets si le système d'exploitation est de type Debian.
2. **Installation de Docker**: Installe Docker.io si le système d'exploitation est de type Debian.
3. **Démarrage et activation du service Docker**: Démarre et active le service Docker.
4. **Ajout de l'utilisateur actuel au groupe Docker**: Permet à l'utilisateur actuel de gérer Docker sans utiliser sudo.

## Déploiement du service NFS sur srvdb1

- Ce bloc de tâches déploie le service NFS sur le serveur nommé srvdb1.

### Tâches:
1. **Mise à jour des paquets**: Met à jour et met à niveau les paquets disponibles.
2. **Ajout de l'utilisateur actuel au groupe Docker**: Ajoute l'utilisateur actuel au groupe Docker.
3. **Installation du serveur NFS**: Installe le serveur NFS.
4. **Création du répertoire à partager**: Crée un répertoire à partager pour le service NFS.
5. **Configuration du partage NFS**: Configure le partage NFS dans le fichier d'exports.
6. **Redémarrage du service NFS**: Redémarre le service NFS après la configuration.

## Configuration sur srvweb1

- Ce bloc de tâches effectue différentes configurations sur le serveur nommé srvweb1.

### Tâches:
1. **Création d'un dossier pour docker-compose**: Crée un dossier pour docker-compose.
2. **Création d'un dossier pour le partage NFS**: Crée un dossier pour le partage NFS.

## Configuration du partage NFS sur srvdb1 et srvweb1

- Ces blocs de tâches configurant le partage NFS sur srvdb1 et srvweb1.

### Tâches:
1. **Récupération de l'adresse IP de srvdb1**: Récupère l'adresse IP de srvdb1 pour la configuration du partage NFS sur srvweb1.
2. **Configuration du partage NFS**: Configure le partage NFS sur srvweb1 en utilisant l'adresse IP récupérée.

## Configuration Docker sur le serveur distant

- Ce bloc de tâches configure Docker sur le serveur nommé srvweb1.

### Tâches:
1. **Création d'un réseau Docker**: Crée un réseau Docker sur le serveur distant.

## Déploiement de docker-compose PostgreSQL sur le serveur distant

- Ce bloc de tâches déploie docker-compose PostgreSQL sur le serveur nommé srvweb1.

### Tâches:
1. **Copie du fichier docker-compose.yml**: Copie le fichier docker-compose.yml sur le serveur distant.
2. **Exécution de docker-compose up**: Exécute docker-compose up sur le serveur distant pour démarrer les services.

## Sauvegarde de la base de données PostgreSQL et configuration de la crontab

- Ce bloc de tâches effectue la sauvegarde de la base de données PostgreSQL et configure la crontab sur le serveur nommé srvweb1.

### Variables:
- `script_name`: Nom du script de sauvegarde.
- `nfs_backup_dir`: Répertoire de sauvegarde sur le serveur NFS.
- `odoo_user`: Nom de l'utilisateur Odoo.
- `odoo_db`: Nom de la base de données Odoo.

### Tâches:
1. **Création d'un dossier pour les scripts sur le serveur distant**: Crée un dossier pour les scripts sur le serveur distant.
2. **Copie du script de sauvegarde sur le serveur**: Copie le script de sauvegarde sur le serveur distant et le rend exécutable.
3. **Création du répertoire de sauvegarde sur le serveur NFS**: Crée le répertoire de sauvegarde sur le serveur NFS.
4. **Ajout d'une tâche cron pour la sauvegarde de la base de données**: Ajoute une tâche cron pour exécuter le script de sauvegarde à minuit tous les jours.
